public class Television {
	public int size;
	public int resolution;
	public String brand;
	
	public void mode() {
		if (size>=75){
			System.out.println("Your TV is in Cinema Mode");
		}
		else if (size<75 && size>=45){
			System.out.println("Your TV is in Sports Mode");
		}
		else {
			System.out.println("Your TV is in Game Mode");
		}
	}
	public void openApp() {
		if (resolution>=1080){
			System.out.println("Opening Netflix...");
		}
		else if (resolution>=720 && resolution<1080) {
			System.out.println("Opening Roku...");
		}
		else if (resolution>=480 && resolution<720) {
			System.out.println("Opening YouTube...");
		}
		else{
			System.out.println("Opening Prime Video...");
		}
		
	}
}
	