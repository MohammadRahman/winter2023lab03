import java.util.Scanner;
public class ApplianceStore {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		Television[] Tv = new Television[4];
		for (int i = 0; i < Tv.length; i++) {
			Tv[i] = new Television();
			System.out.println("Enter the screen size (in inch): ");
			Tv[i].size = reader.nextInt(); 
			System.out.println("Enter the resolution (480?, 720?, 1080? etc): ");
			Tv[i].resolution = reader.nextInt();
			System.out.println("Enter the brand: ");
			Tv[i].brand = reader.next();
		}
		System.out.println("The last TV size (in inch): " + Tv[Tv.length - 1].size + ", the last Tv resolution (in pixel): " + Tv[Tv.length - 1].resolution + ", the last Tv brand: " + Tv[Tv.length - 1].brand);
		Tv[0].mode();
		Tv[0].openApp();
	}
}